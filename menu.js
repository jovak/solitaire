
var bluecross;
var menumusic;

var distance = 300;
var speed = 4;
var cards = [];
var texture;

var cardnumber = 0;
var max = 25;
var xx = [];
var yy = [];
var zz = [];

var Menu = {

    preload : function() {
        // Load all the needed resources for the menu.
        game.load.image("startbutton","buttons/green_button04.png");
        game.load.image("King1","deck/013.png");
        game.load.image("King2","deck/113.png");
        game.load.image("King3","deck/213.png");
        game.load.image("King4","deck/313.png");
        game.load.image("checkBox","buttons/grey_box.png");
        game.load.image("bluecross","buttons/blue_cross.png");
        game.load.audio('menumusic', 'sounds/menu.mp3');
    },

    create: function () {

        game.stage.backgroundColor = '#182d3b';
        menumusic = game.add.audio('menumusic');
        menumusic.loopFull();

        /*// Emitter for some nice card throwing effects
        var emitter = game.add.emitter(game.world.centerX, game.world.height/3, 250);

        emitter.makeParticles(['King1','King2','King3','King4']);

        emitter.minParticleSpeed.setTo(-400, -400);
        emitter.maxParticleSpeed.setTo(500, 500);
        emitter.gravity = 400;
        emitter.start(false, 3000, 660); */

          cards[0] = game.make.sprite(0, 0, 'King1');
          cards[1] = game.make.sprite(0, 0, 'King2');
          cards[2] = game.make.sprite(0, 0, 'King3');
          cards[3] = game.make.sprite(0, 0, 'King4');

          cards[0].scale.setTo(0.8,0.8);
          cards[1].scale.setTo(0.5,0.5);
          cards[2].scale.setTo(0.65,0.65);
          cards[3].scale.setTo(0.5,0.5);

        texture = game.add.renderTexture(1000, 800, 'texture');

        game.add.sprite(0, 0, texture);

    for (var i = 0; i < max; i++)
    {
        xx[i] = Math.floor(Math.random() * 800) - 400;
        yy[i] = Math.floor(Math.random() * 600) - 300;
        zz[i] = Math.floor(Math.random() * 1700) - 100;
    }


        // Create all the buttons
        var startbutton = game.add.button(game.world.centerX, game.world.centerY + 60, 'startbutton', this.actionOnButtonClick, this, 2, 1, 0);
        startbutton.scale.setTo(2, 2);
        startbutton.anchor.setTo(0.5, 0.5);
        var checkBox = game.add.button(game.world.centerX, game.world.height - game.world.height/6 , 'checkBox', this.actionOnCheckBoxClick, this, 2, 1, 0);
        checkBox.scale.setTo(1.5, 1.5);
        checkBox.anchor.setTo(0.5, 0.5);
        bluecross = game.add.sprite(game.world.centerX, game.world.height - game.world.height/6 , 'bluecross');
        bluecross.scale.setTo(1.5, 1.5);
        bluecross.anchor.setTo(0.5, 0.5);
       

        //Create all the different styles for text
        var buttonstyle = { font: "48px ComicSans", fill: "black", wordWrap: true, wordWrapWidth: startbutton.width, align: "center" };
        var headerstyle = { font: "80px ComicSans", fill: "white", wordWrap: true, wordWrapWidth: game.world.width, align: "center" };
        var musicstyle = { font: "40px ComicSans", fill: "red", wordWrap: true, wordWrapWidth: game.world.width, align: "center" };
        
        // Create all text displayed 
        var buttontext = game.add.text(startbutton.x, startbutton.y, "Start Game", buttonstyle);
        buttontext.anchor.set(0.5);
        var headertext = game.add.text(game.world.centerX, game.world.height/8, "Solitaire Version 2.1", headerstyle);
        headertext.anchor.set(0.5);
        var musictext = game.add.text(checkBox.x, checkBox.y - 70, "Play Music", musicstyle);
        musictext.anchor.set(0.5);


    },


    actionOnButtonClick: function() {
        menumusic.stop();
        this.game.state.start('Game');
    },

    actionOnCheckBoxClick: function() {

        // toggle checked box
        gamemusic = !gamemusic;
        if(gamemusic) {
            bluecross = game.add.sprite(game.world.centerX, game.world.height - game.world.height/6 , 'bluecross');
            bluecross.scale.setTo(1.5, 1.5);
            bluecross.anchor.setTo(0.5, 0.5);
            menumusic.resume();
        }
        else {
            bluecross.destroy();
            menumusic.pause();
        }
    },

    getRandomInt:function(min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    },

    update:function() {
       texture.clear();
        for (var i = 0; i < max; i++) {
            var perspective = distance / (distance - zz[i]);
            var x = game.world.centerX + xx[i] * perspective;
            var y = game.world.centerY + 60 -  + yy[i] * perspective;

            zz[i] += speed;

            if (zz[i] > 300) {
                zz[i] -= 600;
            }
            texture.renderXY(cards[2], x, y);
           
            

        }

    }


};
