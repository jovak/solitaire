/* The main game file with the global variables for gamesound and music
 */
var game;


var gamemusic = true;

var gamesoundeffects = true;

game = new Phaser.Game(1000, 800, Phaser.CANVAS, '');


game.state.add('Menu', Menu);

// Adding the Game state.
game.state.add('Game', Game);

game.state.start('Menu');