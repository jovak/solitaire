//window.onload = function() {
/*  // CONSTANTS (not really, 'cause ES5 lacks constant handling) for SUIT and VALUE of a card and because we don't need them, they are commented
    	var DIAMOND = 0;
    	var HEART = 1;
    	var SPADE = 2;
    	var CLUB = 3;

    	var ACE = 1;
    	var JACK = 11;
    	var QUEEN = 12;
    	var KING = 13; */

    // Arrays for talon(new hidden card deck), pile(open cards next to talon), dash(sort by alternate color), block(sorted by suit)
    var talon = [];
    var pile = [];
    var dash = [
        [],
        [],
        [],
        [],
        [],
        [],
        []
    ];
    var block = [
        [],
        [],
        [],
        []
    ];
    var deck = [];
    var taloncover;
    var talonEmpty = 0;
    var dashEmpty = [];
    var blockEmpty = [];

    //nice paddings for card positioning
    var paddingLeft = 14;
    var paddingLeftBlock = 350;
    var paddingBetweenBlocks = 112;
    var paddingBetweenDashs = 112;
    var paddingBetweenTalon = 112;
    var paddingTop = 14;
    var paddingTopDashs = 180;
    var paddingBetweenCards = 20;

    //variables for dragging cards around
    var isDragging = false;
    var dragSprite = false;
    var isColliding = false;
    var isValidMove = false;
    var startPile = 0;
    var endPile = 0;


    var toggleBG = false;
    var prevPilenumber = -2;
    var winonce = true;

    // Soundvariables
    var drawcardsound;
    var victorysound;
    var wrongmovesound;
    var movetoblocksound;
    var testsong;

var Game = {
    


    preload:function() {

        var cardNr = 0;
        var color = 0;
        /* Simply preload all card image files */
        for (var suitNr = 0; suitNr < 4; suitNr++) {
            if (suitNr > 1) {
                color = 1;
            }
            for (var valueNr = 1; valueNr <= 13; valueNr++) {
                deck[cardNr] = new Object();
                deck[cardNr].suit = suitNr;
                deck[cardNr].value = valueNr;
                deck[cardNr].color = color; // 0 = red, 1 = black
                deck[cardNr].hidden = 1;
                deck[cardNr].pilenumber = -1;
                deck[cardNr].sprite = new Object();
                cardNr++;
                game.load.image('' + suitNr + valueNr, 'deck/' + suitNr + valueNr + '.png');
            }
        }
        game.load.audio('drawcardsound', 'sounds/cardsound.mp3');
        game.load.audio('wrongmove', 'sounds/wrongmove.mp3');
        game.load.audio('victory1', 'sounds/victory1.mp3');
        game.load.audio('validmove', 'sounds/validmove.mp3');
        game.load.audio('testsong', 'sounds/testsong.mp3')
        game.load.image('cover', 'deck/cover.png');
        game.load.image('empty', 'deck/empty.png');
        game.load.image('emptyTalon', 'deck/emptyTalon.png');

    },


    create:function() {

        game.physics.startSystem(Phaser.Physics.ARCADE);
        //game.stage.backgroundColor = '#40474C';
        drawcardsound = game.add.audio('drawcardsound');
        victorysound = game.add.audio('victory1');
        wrongmovesound = game.add.audio('wrongmove');
        movetoblocksound = game.add.audio('validmove');
        testsong = game.add.audio("testsong");
        this.initDeck();
        this.setUpStage();

    },

    update:function() {

        var isCollidingPrev = isColliding;
        isColliding = false;
        isValidMove = false;

        // END OF GAM
        if ((block[0].length == 13) && (block[1].length == 13) && (block[2].length == 13) && (block[3].length == 13)) {
            //game.stage.backgroundColor = Math.random() * 0xffffff;
          /*  for (var currentBlock = 0; currentBlock < 4; currentBlock++) {
                for (var currentCard = 0; currentCard < 13; currentCard++) {

                    if (Math.random() > .5) block[currentBlock][currentCard].sprite.x = block[currentBlock][currentCard].sprite.x + Math.round(Math.random() * 5) - 3;
                    else block[currentBlock][currentCard].sprite.y = block[currentBlock][currentCard].sprite.y + Math.round(Math.random() * 5) - 2;
                }
            } */

            // Remove if clause to Add annoying echoing sound effects
            if (winonce) {
            	var tweenmovement;
            	var changedirx = 1;
                for (var currentBlock = 0; currentBlock < 4; currentBlock++) {
                	for (var currentCard = 0; currentCard < 13; currentCard++) {
                    	tweenmovement = game.add.tween(block[currentBlock][currentCard].sprite).to({
                					x: 1000 * Math.random() * changedirx,
                					y: 1000 * Math.random() 
            						}, 1000).loop(true);
                    	changedirx = changedirx * -1;
                    	tweenmovement.start();
                	}
                }
                winonce = false;
                //sprite.body.collideWorldBounds = true;
                /* for (var currentBlock = 0; currentBlock < 4; currentBlock++) {
                    for (var currentCard = 0; currentCard < 13; currentCard++) {
                        block[currentBlock][currentCard].sprite.body.collideWorldBounds = true;
                    }
                } */

            }
        } else {
            // Enable Collision Detection for movable cards (cards on top) and emptyDash/Block
            for (var dashStart = 0; dashStart < 7; dashStart++) {

                for (var dashEnd = 0; dashEnd < dashStart; dashEnd++) {

                    // Colliding Cards need to bee from different dashes
                    if (dashStart != dashEnd) {

                        // startDash and endDash not empty
                        if ((dash[dashStart].length > 0) && (dash[dashEnd].length > 0)) {
                            game.physics.arcade.collide(dash[dashStart][dash[dashStart].length - 1].sprite, dash[dashEnd][dash[dashEnd].length - 1].sprite, this.dashCollisionHandler, null, this);
                        }
                        // startDash empty but endDash not empty
                        else if ((dash[dashStart].length == 0) && (dash[dashEnd].length > 0)) {
                            game.physics.arcade.collide(dashEmpty[dashStart], dash[dashEnd][dash[dashEnd].length - 1].sprite, this.dashCollisionHandler, null, this);
                        }
                        // startDash not empty but endDash empty
                        else if ((dash[dashStart].length > 0) && (dash[dashEnd].length == 0)) {
                            game.physics.arcade.collide(dash[dashStart][dash[dashStart].length - 1].sprite, dashEmpty[dashEnd], this.dashCollisionHandler, null, this);
                        }

                    }

                }

                for (var i = 0; i < 4; i++) {
                    // dash and block not empty
                    if ((dash[dashStart].length > 0) && (block[i].length > 0)) {
                        game.physics.arcade.collide(dash[dashStart][dash[dashStart].length - 1].sprite, block[i][block[i].length - 1].sprite, this.dashCollisionHandler, null, this);
                    }
                    // dash empty but block not empty
                    else if ((dash[dashStart].length == 0) && (block[i].length > 0)) {
                        game.physics.arcade.collide(dashEmpty[dashStart], block[i][block[i].length - 1].sprite, this.dashCollisionHandler, null, this);
                    }
                    // dash not empty but block empty
                    else if ((dash[dashStart].length > 0) && (block[i].length == 0)) {
                        game.physics.arcade.collide(dash[dashStart][dash[dashStart].length - 1].sprite, blockEmpty[i], this.dashCollisionHandler, null, this);
                    }
                }
                // pile to dash
                // dash and pile not empty
                if ((dash[dashStart].length > 0) && (pile.length > 0)) {
                    game.physics.arcade.collide(dash[dashStart][dash[dashStart].length - 1].sprite, pile[pile.length - 1].sprite, this.dashCollisionHandler, null, this);
                }
                // dash empty but pile not empty
                else if ((dash[dashStart].length == 0) && (pile.length > 0)) {
                    game.physics.arcade.collide(dashEmpty[dashStart], pile[pile.length - 1].sprite, this.dashCollisionHandler, null, this);
                }
            }

            for (var blockNr = 0; blockNr < 4; blockNr++) {
                // pile to block
                // block and pile not empty
                if ((block[blockNr].length > 0) && (pile.length > 0)) {
                    game.physics.arcade.collide(block[blockNr][block[blockNr].length - 1].sprite, pile[pile.length - 1].sprite, this.dashCollisionHandler, null, this);
                }
                // block empty but pile not empty
                else if ((block[blockNr].length == 0) && (pile.length > 0)) {
                    game.physics.arcade.collide(blockEmpty[blockNr], pile[pile.length - 1].sprite, this.dashCollisionHandler, null, this);
                }

                // block to block
                for (var endBlockNr = 0; endBlockNr < blockNr; endBlockNr++) {
                    if (blockNr != endBlockNr) {
                        // startBlock and endBlock not empty
                        if ((block[blockNr].length > 0) && (block[endBlockNr].length > 0)) {
                            game.physics.arcade.collide(block[blockNr][block[blockNr].length - 1].sprite, block[endBlockNr][block[endBlockNr].length - 1].sprite, this.dashCollisionHandler, null, this);
                        }
                        // startBlock empty but endBlock not empty
                        else if ((block[blockNr].length == 0) && (block[endBlockNr].length > 0)) {
                            game.physics.arcade.collide(blockEmpty[blockNr], block[endBlockNr][block[endBlockNr].length - 1].sprite, this.dashCollisionHandler, null, this);
                        }
                        // startBlock not empty but endBlock empty
                        else if ((block[blockNr].length > 0) && (block[endBlockNr].length == 0)) {
                            game.physics.arcade.collide(block[blockNr][block[blockNr].length - 1].sprite, blockEmpty[endBlockNr], this.dashCollisionHandler, null, this);
                        }
                    }
                }
            }
            // highlight possible moves
            if (isCollidingPrev && (isColliding === false)) {
                // remove highlight
                dragSprite.tint = 0xffffff;

            } else if (isColliding && isValidMove) {
                // add highlight
                dragSprite.tint = 0xbbffbb;
            }
        }
    },

    // Get a random card (suit between 0-3 and value between 1-13)
    randCard:function() {
        var newCard = deck.splice(Math.floor(Math.random() * (deck.length)), 1);
        return newCard[0];
    },

    // Initialize card deck
    initDeck:function () {
        for (var cardNr = 0; cardNr < 24; cardNr++) {
            talon[cardNr] = this.randCard();
        }

        for (var dashNr = 0; dashNr < 7; dashNr++) {
            for (var cardNr = 0; cardNr <= dashNr; cardNr++) {
                dash[dashNr][cardNr] = this.randCard();
                dash[dashNr][cardNr].pilenumber = dashNr;
            }

        }

    },

    openCard:function (card) {
        drawcardsound.play();
        var newSprite = new Object();
        newSprite = game.add.sprite(card.sprite.x, card.sprite.y, card.suit + '' + card.value);
        newSprite.cardSuit = card.suit;
        newSprite.cardValue = card.value;
        newSprite.pilenumber = card.pilenumber;
        card.sprite.destroy();
        card.sprite = newSprite;
        card.hidden = 0;
        game.physics.enable(card.sprite, Phaser.Physics.ARCADE);
        this.setSmallerBoundaries(card.sprite);
        card.sprite.inputEnabled = true;

    },

    onDragStart:function (sprite) {

        isDragging = true;
        dragSprite = sprite;

    },



    onDragStop:function (sprite, pointer, xorigin, yorigin) {
        isDragging = false;
        if (isColliding && this.checkValidMove(startPile, endPile)) {
            this.doValidMove(startPile, endPile);
        } else {

            sprite.reset(xorigin, yorigin);
        }
    },

    endSingleMoveAnimation:function (tween, pointer, card, startpilenumber, endpilenumber) {
        console.log("Hallo end move!");
        var index;
        if (endpilenumber > 6) {
            /* 
            Add card to block after animation is finished
            */
            movetoblocksound.play();
            console.log("if" + endpilenumber);
            index = endpilenumber - 7;
            block[index].push(card);
            block[index][block[index].length - 1].pilenumber = endpilenumber;
            block[index][block[index].length - 1].sprite.pilenumber = endpilenumber;
            block[index][block[index].length - 1].sprite.inputEnabled = true;
            block[index][block[index].length - 1].sprite.input.enableDrag(false, true);
            block[index][block[index].length - 1].sprite.events.onDragStop.removeAll();
            block[index][block[index].length - 1].sprite.events.onDragStart.removeAll();
            block[index][block[index].length - 1].sprite.events.onDragStop.add(this.onDragStop, this, 100, blockEmpty[index].x, blockEmpty[index].y);
            block[index][block[index].length - 1].sprite.events.onDragStart.add(this.onDragStart, this);
            block[index][block[index].length - 1].sprite.events.onInputDown.add(this.doubleTapHandler, this);
            /* 
            If card came from subdesh look for facedown card and open it
    		*/
            if (startpilenumber >= 0 && startpilenumber < 7) {
                if (dash[startpilenumber].length !== 0) {
                    if (dash[startpilenumber][dash[startpilenumber].length - 1].hidden === 1) {
                        this.openCard(dash[startpilenumber][dash[startpilenumber].length - 1]);
                    }
                    dash[startpilenumber][dash[startpilenumber].length - 1].pilenumber = startpilenumber;
                    dash[startpilenumber][dash[startpilenumber].length - 1].sprite.pilenumber = startpilenumber;
                    dash[startpilenumber][dash[startpilenumber].length - 1].sprite.inputEnabled = true;
                    dash[startpilenumber][dash[startpilenumber].length - 1].sprite.input.enableDrag(false, true);
                    dash[startpilenumber][dash[startpilenumber].length - 1].sprite.events.onDragStart.removeAll();
                    dash[startpilenumber][dash[startpilenumber].length - 1].sprite.events.onDragStop.removeAll();
                    dash[startpilenumber][dash[startpilenumber].length - 1].sprite.events.onDragStop.add(this.onDragStop, this, 100, paddingLeft + paddingBetweenDashs * startpilenumber, (paddingTopDashs + paddingBetweenCards * (dash[startpilenumber].length - 1)));
                    dash[startpilenumber][dash[startpilenumber].length - 1].sprite.events.onDragStart.add(this.onDragStart, this);
                    dash[startpilenumber][dash[startpilenumber].length - 1].sprite.events.onInputDown.add(this.doubleTapHandler, this);
                }
            }

        } else if (endpilenumber < 7 && endpilenumber >= 0) {
            dash[endpilenumber].push(card);
            dash[endpilenumber][dash[endpilenumber].length - 1].pilenumber = endpilenumber;
            dash[endpilenumber][dash[endpilenumber].length - 1].sprite.pilenumber = endpilenumber;
            dash[endpilenumber][dash[endpilenumber].length - 1].sprite.inputEnabled = true;
            dash[endpilenumber][dash[endpilenumber].length - 1].sprite.input.enableDrag(false, true);
            dash[endpilenumber][dash[endpilenumber].length - 1].sprite.events.onDragStop.removeAll();
            dash[endpilenumber][dash[endpilenumber].length - 1].sprite.events.onDragStart.removeAll();
            dash[endpilenumber][dash[endpilenumber].length - 1].sprite.events.onDragStop.add(this.onDragStop, this, 100, paddingLeft + paddingBetweenDashs * endpilenumber, (paddingTopDashs + paddingBetweenCards * (dash[endpilenumber].length - 1)));
            dash[endpilenumber][dash[endpilenumber].length - 1].sprite.events.onDragStart.add(this.onDragStart, this);
            dash[endpilenumber][dash[endpilenumber].length - 1].sprite.events.onInputDown.add(this.doubleTapHandler, this);
        }
    },

   endMultipleMoveAnimation:function(tween, pointer, presortedcards, startpilenumber, endpilenumber) {
        if (dash[endpilenumber].length !== 0) {
            dash[endpilenumber][dash[endpilenumber].length - 1].sprite.inputEnabled = false;
        }
        dash[endpilenumber].push.apply(dash[endpilenumber], presortedcards);
        if (dash[startpilenumber].length !== 0) {
            if (dash[startpilenumber][dash[startpilenumber].length - 1].hidden === 1) {
                this.openCard(dash[startpilenumber][dash[startpilenumber].length - 1]);
            }
            dash[startpilenumber][dash[startpilenumber].length - 1].pilenumber = startpilenumber;
            dash[startpilenumber][dash[startpilenumber].length - 1].sprite.pilenumber = startpilenumber;
            dash[startpilenumber][dash[startpilenumber].length - 1].sprite.inputEnabled = true;
            dash[startpilenumber][dash[startpilenumber].length - 1].sprite.input.enableDrag(false, true);
            dash[startpilenumber][dash[startpilenumber].length - 1].sprite.events.onDragStart.removeAll();
            dash[startpilenumber][dash[startpilenumber].length - 1].sprite.events.onDragStop.removeAll();
            dash[startpilenumber][dash[startpilenumber].length - 1].sprite.events.onDragStop.add(this.onDragStop, this, 100, paddingLeft + paddingBetweenDashs * startpilenumber, (paddingTopDashs + paddingBetweenCards * (dash[startpilenumber].length - 1)));
            dash[startpilenumber][dash[startpilenumber].length - 1].sprite.events.onDragStart.add(this.onDragStart, this);
            dash[startpilenumber][dash[startpilenumber].length - 1].sprite.events.onInputDown.add(this.doubleTapHandler, this);
        }
        dash[endpilenumber][dash[endpilenumber].length - 1].sprite.inputEnabled = true;
        dash[endpilenumber][dash[endpilenumber].length - 1].sprite.input.enableDrag(false, true);
        dash[endpilenumber][dash[endpilenumber].length - 1].sprite.events.onDragStop.removeAll();
        dash[endpilenumber][dash[endpilenumber].length - 1].sprite.events.onDragStart.removeAll();
        dash[endpilenumber][dash[endpilenumber].length - 1].sprite.events.onDragStop.add(this.onDragStop, this, 100, paddingLeft + paddingBetweenDashs * endpilenumber, (paddingTopDashs + paddingBetweenCards * (dash[endpilenumber].length - 1)));
        dash[endpilenumber][dash[endpilenumber].length - 1].sprite.events.onDragStart.add(this.onDragStart, this);
        dash[endpilenumber][dash[endpilenumber].length - 1].sprite.events.onInputDown.add(this.doubleTapHandler, this);



    },

    doubleTapHandler:function (sprite, pointer) {
        var validmove = false;
        if (pointer.msSinceLastClick < game.input.doubleTapRate) {
            console.log('Double clicked sprite: ', sprite.key);
            for (var i = 0; i < blockEmpty.length; i++) {
                validmove = this.checkValidMove(sprite.pilenumber, blockEmpty[i].pilenumber);
                if (validmove) {
                    this.doValidMove(sprite.pilenumber, blockEmpty[i].pilenumber);
                    i = blockEmpty.length;
                }
            }
        }
    },

    dashCollisionHandler:function (sprite1, sprite2) {

        isColliding = true;

        if (sprite1 === dragSprite) {
            startPile = sprite1.pilenumber;
            endPile = sprite2.pilenumber;
        } else if (sprite2 === dragSprite) {
            startPile = sprite2.pilenumber;
            endPile = sprite1.pilenumber;
        } else if (dragSprite == false) {
            console.log("Problem in dashCollisionHandler(): dragSprite = false");
        } else {
            console.log("Problem in dashCollisionHandler()");
        }

        if (prevPilenumber !== endPile) {
            prevPilenumber = endPile;
            isValidMove = this.checkValidMove(startPile, endPile);
        }


    },


    setSmallerBoundaries:function (sprite) {
        // Set smaller boundaries
        var boundWidth = sprite.width * 0.6;
        var boundHeight = sprite.height * 0.7;
        var boundOffsetX = sprite.width * 0.3;
        var boundOffsetY = sprite.height * 0.35;
        sprite.body.setSize(boundWidth, boundHeight, boundOffsetX, boundOffsetY);
    },

    setUpStage:function () {

        if(gamemusic) {
            testsong.play();
        }
        /* Talon Kartenstapel */
        taloncover = game.add.sprite(paddingLeft, paddingTop, 'cover');
        taloncover.anchor.setTo(0, 0);
        //enables all kind of input actions on this image (click, etc)
        taloncover.inputEnabled = true;
        taloncover.events.onInputDown.add(this.getTalonCard, this);
        //taloncover.input.enableDrag(false, true);

        /* Endablage Leer 1-4*/
        for (var i = 0; i < 4; i++) {
            blockEmpty[i] = game.add.sprite(paddingLeftBlock + i * paddingBetweenBlocks, paddingTop, 'empty');
            blockEmpty[i].pilenumber = i + 7;
            blockEmpty[i].anchor.setTo(0, 0);
            blockEmpty[i].enableBody = true;
            game.physics.enable(blockEmpty[i], Phaser.Physics.ARCADE);

            // Enable collision detection prototyes
            blockEmpty[i].inputEnabled = true;

            // Set smaller boundaries
            this.setSmallerBoundaries(blockEmpty[i]);
        }

        /* Sortierstapel Leer 1-7 */
        for (var i = 0; i < 7; i++) {
            dashEmpty[i] = game.add.sprite(paddingLeft + i * paddingBetweenDashs, paddingTopDashs, 'empty');
            dashEmpty[i].pilenumber = i;
            dashEmpty[i].anchor.setTo(0, 0);
            dashEmpty[i].enableBody = true;
            game.physics.enable(dashEmpty[i], Phaser.Physics.ARCADE);

            // Enable collision detection prototyes
            dashEmpty[i].inputEnabled = true;

            // Set smaller boundaries
            this.setSmallerBoundaries(dashEmpty[i]);
        }

        // Sortierstapel 1-7
        var newCardName;

        for (var dashNr = 0; dashNr < 7; dashNr++) {

            for (var cardNr = 0; cardNr < dash[dashNr].length; cardNr++) {
                newCardName = dash[dashNr][cardNr].suit + '' + dash[dashNr][cardNr].value;
                if (cardNr === (dash[dashNr].length - 1)) {

                    dash[dashNr][cardNr].sprite = game.add.sprite(paddingLeft + paddingBetweenDashs * dashNr, (paddingTopDashs + paddingBetweenCards * (cardNr)), newCardName);
                    dash[dashNr][cardNr].sprite.cardSuit = dash[dashNr][cardNr].suit;
                    dash[dashNr][cardNr].sprite.cardValue = dash[dashNr][cardNr].value;
                    dash[dashNr][cardNr].sprite.pilenumber = dash[dashNr][cardNr].pilenumber;
                    dash[dashNr][cardNr].sprite.anchor.setTo(0, 0);
                    dash[dashNr][cardNr].sprite.inputEnabled = true;
                    dash[dashNr][cardNr].sprite.input.enableDrag(false, true);
                    dash[dashNr][cardNr].sprite.enableBody = true;
                    dash[dashNr][cardNr].sprite.events.onDragStop.add(this.onDragStop, this, 100, paddingLeft + paddingBetweenDashs * dashNr, (paddingTopDashs + paddingBetweenCards * cardNr));
                    dash[dashNr][cardNr].sprite.events.onDragStart.add(this.onDragStart, this);
                    dash[dashNr][cardNr].sprite.events.onInputDown.add(this.doubleTapHandler, this);
                    game.physics.enable(dash[dashNr][cardNr].sprite, Phaser.Physics.ARCADE);
                    // Set Sprite to be face up
                    dash[dashNr][cardNr].hidden = 0;
                    this.setSmallerBoundaries(dash[dashNr][cardNr].sprite);

                } else {
                    // uncomment for open cards
                    //dash[dashNr][cardNr].sprite = game.add.sprite(paddingLeft + paddingBetweenDashs * dashNr, (paddingTopDashs + paddingBetweenCards * (cardNr)), newCardName);
                    dash[dashNr][cardNr].sprite = game.add.sprite(paddingLeft + paddingBetweenDashs * dashNr, (paddingTopDashs + paddingBetweenCards * (cardNr)), 'cover');
                    dash[dashNr][cardNr].sprite.cardSuit = dash[dashNr][cardNr].suit;
                    dash[dashNr][cardNr].sprite.cardValue = dash[dashNr][cardNr].value;
                    dash[dashNr][cardNr].sprite.pilenumber = dash[dashNr][cardNr].pilenumber;
                    dash[dashNr][cardNr].sprite.anchor.setTo(0, 0);
                    dash[dashNr][cardNr].sprite.inputEnabled = true;
                    dash[dashNr][cardNr].sprite.enableBody = true;
                }

            }
        }

    },

    getTalonCard:function () {

        if (talon.length > 0) {
            var popedCard = talon[talon.length - 1];
            console.log('Show new card from talon');
            pile.push(talon.pop());
            pile[pile.length - 1].sprite = game.add.sprite(paddingLeft + paddingBetweenTalon, paddingTop, popedCard.suit + '' + popedCard.value);
            pile[pile.length - 1].sprite.pilenumber = -1;
            pile[pile.length - 1].sprite.anchor.setTo(0, 0);
            pile[pile.length - 1].sprite.inputEnabled = true;
            pile[pile.length - 1].sprite.input.enableDrag(false, true);
            pile[pile.length - 1].sprite.enableBody = true;
            pile[pile.length - 1].sprite.events.onDragStop.add(this.onDragStop, this, 100, paddingLeft + paddingBetweenTalon, paddingTop);
            pile[pile.length - 1].sprite.events.onDragStart.add(this.onDragStart, this);
            pile[pile.length - 1].sprite.events.onInputDown.add(this.doubleTapHandler, this);
            pile[pile.length - 1].hidden = 0;
            game.physics.enable(pile[pile.length - 1].sprite, Phaser.Physics.ARCADE);

            // Set smaller boundaries
            this.setSmallerBoundaries(pile[pile.length - 1].sprite);

            //This allows your sprite to collide with the world bounds like they were rigid objects
            //pile[pile.length - 1].sprite.body.collideWorldBounds = true;

            if (talon.length == 0) {
                taloncover.destroy();
                taloncover = game.add.sprite(paddingLeft, paddingTop, 'emptyTalon');
                taloncover.anchor.setTo(0, 0);
                //enables all kind of input actions on this image (click, etc)
                taloncover.inputEnabled = true;
                taloncover.events.onInputDown.add(this.getTalonCard, this);
            }
        } else {
            taloncover.destroy();
            taloncover = game.add.sprite(paddingLeft, paddingTop, 'cover');
            taloncover.anchor.setTo(0, 0);
            //enables all kind of input actions on this image (click, etc)
            taloncover.inputEnabled = true;
            taloncover.events.onInputDown.add(this.getTalonCard, this);
            for (var spriteNr = 0; spriteNr < pile.length; spriteNr++) {
                pile[spriteNr].sprite.destroy();
            }
            talon = pile.slice().reverse();
            pile = [];
            talonEmpty = 0;
            console.log('Einmal durch. Neue Talonlaenge: ' + talon.length);
        }
    },

    // Looks for a presorted subdash ( by alternate color and ascending/descending values) in the startdash
    // and checks if you can move it to the given enddash 
    checkValidMove:function (startpilenumber, endpilenumber) {
        console.log("Startdash:" + startpilenumber + " Enddash:" + endpilenumber);

        var validmove = false;

        // return false if endpilenumber is equal to startpile
        if (endpilenumber === startpilenumber) {
            validmove = false;
        } else if (endpilenumber === -1) {
            validmove = false;
        }

        // Check move from subdash to subdash
        else if (startpilenumber < 7 && startpilenumber >= 0 && endpilenumber < 7) {

            console.log("Startdash length:" + dash[startpilenumber].length + " Enddash lenght:" + dash[endpilenumber].length);
            var indexofpresorteddash = 0;


            for (var i = 0; i <= dash[startpilenumber].length - 1; i++) {
                if (dash[startpilenumber][i].hidden === 0) {
                    indexofpresorteddash = i;
                    i = 100;
                }
            }

            for (var p = 0; p <= dash[startpilenumber].length - 1; p++) {
                console.log("Value " + dash[startpilenumber][p].value + " Hidden  " + dash[startpilenumber][p].hidden);
            }

            // Check if the endpile is empty or not
            if (dash[endpilenumber].length != 0) {

                var enddashcard = dash[endpilenumber][dash[endpilenumber].length - 1];

                for (var j = indexofpresorteddash; j <= dash[startpilenumber].length - 1; j++) {
                    // if valid card to be moved was found end the loop prematurely
                    // with the found index

                    if (enddashcard.color != dash[startpilenumber][j].color) {
                        if (enddashcard.value === dash[startpilenumber][j].value + 1) {
                            validmove = true;
                            j = 10000;
                        }
                    }
                }
            } else {
                if (dash[startpilenumber][indexofpresorteddash].value === 13) {
                    validmove = true;
                }
            }
        }

        // Check move from block to subdash
        else if (startpilenumber > 6 && endpilenumber < 7) {

            // check if subdash is empty
            if (dash[endpilenumber].length != 0) {

                var enddashcard = dash[endpilenumber][dash[endpilenumber].length - 1];

                if (enddashcard.color != block[startpilenumber - 7][block[startpilenumber - 7].length - 1].color) {
                    if (enddashcard.value === block[startpilenumber - 7][block[startpilenumber - 7].length - 1].value + 1) {
                        validmove = true;
                        j = 10000;
                    }
                }
            } else {
                if (block[startpilenumber - 7][block[startpilenumber - 7].length - 1].value === 13) {
                    validmove = true;
                }
            }
        }

        // Check move from subdash to block
        else if (endpilenumber > 6 && startpilenumber < 7 && startpilenumber >= 0) {

            var startdashcard = dash[startpilenumber][dash[startpilenumber].length - 1]

            // Check if move to empty block or not
            if (block[endpilenumber - 7].length != 0) {

                var enddblockcard = block[endpilenumber - 7][block[endpilenumber - 7].length - 1];

                // Allow move if same suit and bigger value
                if (enddblockcard.suit === startdashcard.suit) {
                    if (enddblockcard.value === startdashcard.value - 1) {
                        validmove = true;
                        j = 10000;
                    }
                }
            }

            // If block is empty only an Ass can be dragged to it
            else {
                if (startdashcard.value === 1) {
                    validmove = true;
                }
            }

        }

        // Check move from block to block
        else if (startpilenumber > 6 && endpilenumber > 6) {

            var startblockcard = block[startpilenumber - 7][block[startpilenumber - 7].length - 1];

            if (block[endpilenumber - 7].length === 0) {
                if (startblockcard.value === 1) {
                    validmove = true;
                }
            }
        }


        // Check move from opencardpile to block or subdash
        else if (startpilenumber === -1) {

            var faceupcard = pile[pile.length - 1];
            // Check move from opencardpile to block
            if (endpilenumber > 6) {

                faceupcard = pile[pile.length - 1];

                // Check if move to empty block or not
                if (block[endpilenumber - 7].length != 0) {

                    var enddblockcard = block[endpilenumber - 7][block[endpilenumber - 7].length - 1];

                    // Allow move if same suit and bigger value
                    if (enddblockcard.suit === faceupcard.suit) {
                        if (enddblockcard.value === faceupcard.value - 1) {
                            validmove = true;
                            j = 10000;
                        }
                    }
                }

                // If block is empty only an Ass can be dragged to it
                else {
                    if (faceupcard.value === 1) {
                        validmove = true;
                    }
                }

            }

            // Check move from opencardpile to subdash
            else {
                if (dash[endpilenumber].length != 0) {

                    var enddashcard = dash[endpilenumber][dash[endpilenumber].length - 1];

                    if (enddashcard.color != faceupcard.color) {
                        if (enddashcard.value === faceupcard.value + 1) {
                            validmove = true;
                        }
                    }
                } else {
                    if (faceupcard.value === 13) {
                        validmove = true;
                    }
                }

            }
        }

        if (validmove) console.log("This move would be valid!");
        if (!validmove) console.log("This move would  N O T  be valid!");
        return validmove;
    },

    doValidMove:function(startpilenumber, endpilenumber) {

        prevPilenumber = -2;

        // Do move from subdash to subdash
        if (startpilenumber < 7 && startpilenumber >= 0 && endpilenumber < 7) {

            console.log("Hallo aus move");
            console.log("Startdash: " + startpilenumber + " Enddash: " + endpilenumber);
            var presorteddash = [];
            var indexofpresorteddash = 0;
            for (var i = 0; i <= dash[startpilenumber].length - 1; i++) {
                if (dash[startpilenumber][i].hidden === 0) {
                    indexofpresorteddash = i;
                    i = 100;
                }
            }
            // Check if the pile is empty
            if (dash[endpilenumber].length !== 0) {
                var enddashcard = dash[endpilenumber][dash[endpilenumber].length - 1];
                var loopcount = 0;

                for (var j = indexofpresorteddash; j <= dash[startpilenumber].length - 1; j++) {
                    // if valid card to be moved was found end the loop prematurely
                    // with the found index
                    console.log("indexofpresorteddash:  " + indexofpresorteddash);
                    console.log("Start Color: " + dash[startpilenumber][j].color);
                    console.log("Start Value: " + dash[startpilenumber][j].value);

                    if (enddashcard.color != dash[startpilenumber][j].color) {
                        console.log("Hallo aus color")
                        if (enddashcard.value === dash[startpilenumber][j].value + 1) {
                            var indexofvalidcard = j;
                            console.log("In if clause!" + indexofvalidcard);
                            j = 10000;
                            for (var k = indexofvalidcard; k <= dash[startpilenumber].length - 1; k++) {
                                loopcount++;
                                var tweenmovement = game.add.tween(dash[startpilenumber][k].sprite).to({
                                    x: enddashcard.sprite.x,
                                    y: enddashcard.sprite.y + paddingBetweenCards * loopcount
                                }, 250);

                                dash[startpilenumber][k].sprite.bringToTop();
                                dash[startpilenumber][k].sprite.inputEnabled = false;

                                if (k === dash[startpilenumber].length - 1) {
                                    var startnumber = dash[startpilenumber][k].pilenumber;
                                    dash[startpilenumber][k].pilenumber = enddashcard.pilenumber;
                                    dash[startpilenumber][k].sprite.pilenumber = enddashcard.sprite.pilenumber;
                                    presorteddash = dash[startpilenumber].splice(indexofvalidcard);
                                    console.log("Index indexofvalidcard" + indexofvalidcard);
                                    console.log("Presorted dash" + presorteddash);
                                    tweenmovement.onComplete.add(this.endMultipleMoveAnimation, this, 100, presorteddash, startnumber, enddashcard.pilenumber);
                                    tweenmovement.start();
                                } else {
                                    dash[startpilenumber][k].pilenumber = enddashcard.pilenumber;
                                    dash[startpilenumber][k].sprite.pilenumber = enddashcard.sprite.pilenumber;

                                    tweenmovement.start();
                                }

                            }

                        }
                    }
                }
            }
            // Only allow King move to empty pile
            else {
                if (dash[startpilenumber][indexofpresorteddash].value === 13) {
                    var emptydashsprite = dashEmpty[endpilenumber];
                    var loopcount = 0;
                    for (var k = indexofpresorteddash; k <= dash[startpilenumber].length - 1; k++) {
                        var tweenmovement = game.add.tween(dash[startpilenumber][k].sprite).to({
                            x: emptydashsprite.x,
                            y: emptydashsprite.y + paddingBetweenCards * loopcount
                        }, 250);

                        dash[startpilenumber][k].sprite.bringToTop();
                        dash[startpilenumber][k].sprite.inputEnabled = false;

                        if (k === dash[startpilenumber].length - 1) {
                            var startnummer = dash[startpilenumber][k].pilenumber;
                            dash[startpilenumber][k].pilenumber = emptydashsprite.pilenumber;
                            dash[startpilenumber][k].sprite.pilenumber = emptydashsprite.pilenumber;
                            presorteddash = dash[startpilenumber].splice(indexofpresorteddash);
                            console.log("Index presorteddash" + indexofpresorteddash);
                            console.log("Presorted dash" + presorteddash);
                            tweenmovement.onComplete.add(this.endMultipleMoveAnimation, this, 100, presorteddash, startnummer, emptydashsprite.pilenumber);
                            tweenmovement.start();
                        } else {
                            dash[startpilenumber][k].pilenumber = emptydashsprite.pilenumber;
                            dash[startpilenumber][k].sprite.pilenumber = emptydashsprite.pilenumber;
                            tweenmovement.start();
                        }

                        loopcount++;

                    }

                }
            }

        }
        // Do move from Subdash to Block
        else if (startpilenumber < 7 && startpilenumber >= 0 && endpilenumber > 6) {

            var emptyblocksprite = blockEmpty[endpilenumber - 7];
            //dash[startpilenumber][dash[startpilenumber].length - 1].sprite.reset(emptyblocksprite.x, emptyblocksprite.y);
            //dash[startpilenumber][dash[startpilenumber].length - 1].sprite.bringToTop();
            var cardtomove = dash[startpilenumber].pop();
            // add Tween to move the sprite
            var tweenmovement = game.add.tween(cardtomove.sprite).to({
                x: emptyblocksprite.x,
                y: emptyblocksprite.y
            }, 250);
            tweenmovement.onComplete.add(this.endSingleMoveAnimation, this, 100, cardtomove, cardtomove.pilenumber, emptyblocksprite.pilenumber);
            tweenmovement.start();
            // block[endpilenumber - 7].push(cardtomove);





        }

        // Do move from Block to Block
        else if (startpilenumber > 6 && endpilenumber > 6) {
            var emptyblocksprite = blockEmpty[endpilenumber - 7];
            var cardtomove = block[startpilenumber - 7].pop();
            var tweenmovement = game.add.tween(cardtomove.sprite).to({
                x: emptyblocksprite.x,
                y: emptyblocksprite.y
            }, 250);
            tweenmovement.onComplete.add(this.endSingleMoveAnimation, this, 100, cardtomove, cardtomove.pilenumber, emptyblocksprite.pilenumber);
            tweenmovement.start();
        }

        // Do move from Pile to Block or SubDash
        else if (startpilenumber === -1) {
            // Move from Pile to Block
            if (endpilenumber > 6) {
                var emptyblocksprite = blockEmpty[endpilenumber - 7];
                var cardtomove = pile.pop();
                var tweenmovement = game.add.tween(cardtomove.sprite).to({
                    x: emptyblocksprite.x,
                    y: emptyblocksprite.y
                }, 250);
                // Add card and sprite to list after animation is done to prevent coll detect errors
                tweenmovement.onComplete.add(this.endSingleMoveAnimation, this, 100, cardtomove, cardtomove.pilenumber, emptyblocksprite.pilenumber);
                tweenmovement.start();


            }
            // Move from Pile to Subdash
            else {
                console.log("Hallo pile to subdash");
                var dashsprite;
                var yoffset = 0;
                if (dash[endpilenumber].length !== 0) {
                    dashsprite = dash[endpilenumber][dash[endpilenumber].length - 1].sprite;
                    dashsprite.inputEnabled = false;
                    yoffset = paddingBetweenCards;
                } else {
                    dashsprite = dashEmpty[endpilenumber];

                }
                //pile[pile.length - 1].sprite.reset(dashsprite.x, dashsprite.y + yoffset);
                //pile[pile.length - 1].sprite.bringToTop();
                var cardtomove = pile.pop();
                var tweenmovement = game.add.tween(cardtomove.sprite).to({
                    x: dashsprite.x,
                    y: dashsprite.y + yoffset
                }, 250);
                tweenmovement.onComplete.add(this.endSingleMoveAnimation, this, 100, cardtomove, cardtomove.pilenumber, dashsprite.pilenumber);
                tweenmovement.start();



            }
        }

        // Do move from Block to SubDash
        else if (startpilenumber > 6 && endpilenumber < 7 && endpilenumber >= 0) {
            var dashsprite;
            var yoffset = 0;
            if (dash[endpilenumber].length !== 0) {
                dashsprite = dash[endpilenumber][dash[endpilenumber].length - 1].sprite;
                yoffset = paddingBetweenCards;
            } else {
                dashsprite = dashEmpty[endpilenumber];
            }
            //block[startpilenumber -7][block[startpilenumber -7].length-1].sprite.reset(dashsprite.x, dashsprite.y + yoffset);
            //block[startpilenumber -7][block[startpilenumber -7].length-1].sprite.bringToTop();
            block[startpilenumber - 7][block[startpilenumber - 7].length - 1].sprite.pilenumber = dashsprite.pilenumber;
            block[startpilenumber - 7][block[startpilenumber - 7].length - 1].pilenumber = dashsprite.pilenumber;
            block[startpilenumber - 7][block[startpilenumber - 7].length - 1].sprite.inputEnabled = false;
            var cardtomove = block[startpilenumber - 7].pop();
            var tweenmovement = game.add.tween(cardtomove.sprite).to({
                x: dashsprite.x,
                y: dashsprite.y + yoffset
            }, 250);
            tweenmovement.onComplete.add(this.endSingleMoveAnimation, this, 100, cardtomove, cardtomove.pilenumber, dashsprite.pilenumber);
            tweenmovement.start();
        }




    },

    restartGame:function() {
        this.game.state.start('Game');
        testsong.stop();
    }

};